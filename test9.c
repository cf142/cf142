#include<stdio.h>
struct student
{
	int rno;
	char name[25];
	char section[10];
	char department[25];
	float fees;
	float results;
};

int main()
{
	struct student s1,s2;
	printf("Enter the details of student1:Roll no, name, section, department,fees,results\n");
	scanf("%d%s%s%s%f%f",&s1.rno,s1.name,s1.section,s1.department,&s1.fees,&s1.results);
	printf("Enter the details of student2:Roll no, name, section, department,fees,results\n");
	scanf("%d%s%s%s%f%f",&s2.rno,s2.name,s2.section,s2.department,&s2.fees,&s2.results);

	if(s1.results>s2.results)
	{
		printf("Roll no:%d\n Name:%s\n Section:%s\n Department:%s\n Fees:%f\n Results:%f\n",s1.rno,s1.name,s1.section,s1.department,s1.fees,s1.results);
	}
	else
	{
		printf("Roll no:%d\n Name:%s\n Section:%s\n Department:%s\n Fees:%f\n Results:%f\n",s2.rno,s2.name,s2.section,s2.department,s2.fees,s2.results);
	}

	return 0;
}

