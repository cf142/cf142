#include<stdio.h>
int main()
{
    int marks[5][3],i,j,max=0;
    for(i=0;i<5;i++){
        printf("Student %d :- \n",i+1);
        for(j=0;j<3;j++){
            printf("Enter marks in course %d :",j+1);
            scanf("%d",&marks[i][j]);
            printf("\n");
        }
    }
    for(j=0;j<3;j++){
        for(i=0;i<5;i++){
            if(max<marks[i][j])
                max=marks[i][j];
        }
        printf("Highest marks in course %d = %d \n",j+1,max);
        max=0;
    }
    return 0;
}
