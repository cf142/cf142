#include<stdio.h>
int main()
{
    int sales[4][3],s,p,sum=0;
    for(s=0;s<4;s++){
        for(p=0;p<3;p++){
            printf("Enter the Number of sales for Salesman %d for PRODUCT %d : \n ",s+1,p+1);
            scanf("%d",&sales[s][p]);
        }
        printf("\n");
    }
    printf("\n\n");
    for(s=0;s<4;s++){
        for(p=0;p<3;p++){
            sum+=sales[s][p];
        }
        printf("Total sales of SALESMAN %d = %d",s+1,sum);
        sum=0;
        printf("\n");
    }
    printf("\n\n");
    for(p=0;p<3;p++){
        for(s=0;s<4;s++){
            sum+=sales[s][p];
        }
        printf("Total sales of PRODUCT %d = %d",p+1,sum);
        sum=0;
        printf("\n");
    }
    return 0;
}
