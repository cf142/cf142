#include<stdio.h>
#include<math.h>
int main()
{
int a,b,c;
float x1,x2,disc;
printf("Enter the coefficients of a quadratic equation\n");
scanf("%d%d%d",&a,&b,&c);
disc=((b*b)-(4*a*c));
if(a==0)
{
printf("the roots are invalid\n");
}
else if(disc==0)
{
x1=(-b/(2*a));
x2=(-b/(2*a));
printf("the roots are real and equal\n");
printf("the root is x1=%f\n",x1);
printf("the root is x2=%f\n",x2);
}
else if(disc>0)
{
x1=((-b+sqrt(disc))/2*a);
x2=((-b-sqrt(disc))/2*a);
printf("the roots are real and distinct\n");
printf("the root is x1=%f\n",x1);
printf("the root is x2=%f\n",x2);
}
else
{
x1=(-b/(2*a));
x2=sqrt(fabs(disc));
printf("the roots are complex and distinct\n");
printf("the root is x1=%f+i%f\n",x1,x2);
printf("the root is x2=%f-i%f\n",x1,x2);
}
return 0;
}